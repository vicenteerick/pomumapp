//
//  ServicoTarefa.h
//  BemCasado
//
//  Created by Erick Vicente on 04/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FabricaDeServico.h"
#import "Tarefa.h"

@interface ServicoTarefa : FabricaDeServico

-(NSMutableArray *)listarPor:(int)idCasamento;
-(int)totalPor:(bool)estaConcluido ePorCasamento:(int)idCasamento;
-(double)valorTotalPor:(int)idCasamento;
-(Tarefa *) salvar:(Tarefa *)tarefa ePorCasamento:(int)idCasamento;
-(bool) atualizar:(Tarefa *)tarefa;

@end
