//
//  TarefaViewController.h
//  BemCasado
//
//  Created by Erick Vicente on 05/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tarefa.h"
#import "ServicoTarefa.h"
#import "TarefaCell.h"
#import "TarefaDetalheViewController.h"

@interface TarefaViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    
    UITableView *tabelaDeTarefas;
    NSMutableArray *listaDeTarefas;
    Tarefa *tarefaSelecionada;
    
}

@property (strong, nonatomic) IBOutlet UITableView *tabelaDeTarefas;
@property (strong, nonatomic) NSMutableArray *listaDeTarefas;
@property (strong, nonatomic) Tarefa *tarefaSelecionada;

@end
