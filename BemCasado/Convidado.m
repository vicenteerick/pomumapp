//
//  Convidado.m
//  BemCasado
//
//  Created by Erick Vicente on 24/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "Convidado.h"

@implementation Convidado

-(void) setID:(int)novoID{
    ID = novoID;
}
-(void) setNome:(NSString *)novoNome{
    nome = novoNome;
}
-(void) setTelefone:(NSString *)novaTelefone{
    telefone = novaTelefone;
}
-(void) setEmail:(NSString *)novaEmail{
    email = novaEmail;
}
-(void) setConvidadosAdicionais:(int)novoConvidadosAdicionais{
    convidadosAdicionais = novoConvidadosAdicionais;
}
-(void) setConfirmado:(bool)estaConfirmado{
    confirmado = estaConfirmado;
}

-(int) getID{
    return ID;
}
-(NSString *) getNome{
    return nome;
}
-(NSString *) getTelefone{
    return telefone;
}
-(NSString *) getEmail{
    return email;
}
-(int) getConvidadosAdicionados{
    return convidadosAdicionais;
}
-(bool) estaConfirmado{
    return confirmado;
}

-(NSMutableDictionary *) paraNSDictionary{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    if(self.getID > 0){
        [dictionary setValue:[NSNumber numberWithInteger:self.getID] forKey:@"ID"];
    }
    
    [dictionary setValue:self.getNome forKey:@"nome"];
    [dictionary setValue:self.getTelefone forKey:@"telefone"];
    [dictionary setValue:self.getEmail forKey:@"email"];
    [dictionary setValue:[NSNumber numberWithInteger:self.getConvidadosAdicionados] forKey:@"convidadosAdicionais"];
    [dictionary setValue:[NSNumber numberWithBool:self.estaConfirmado] forKey:@"confirmado"];
    
    return dictionary;
    
}

-(Convidado *)paraOjetoConvidadoDe:(NSDictionary *)dicionario{
    
    [self setID: [[dicionario objectForKey:@"ID"] intValue]];
    [self setNome:[dicionario objectForKey:@"nome"]];
    [self setTelefone:[dicionario objectForKey:@"telefone"]];
    [self setEmail:[dicionario objectForKey:@"email"]];
    [self setConvidadosAdicionais:[[dicionario objectForKey:@"convidadosAdicionais"] intValue]];
    [self setConfirmado:[[dicionario objectForKey:@"confirmado"] boolValue]];
    
    return self;
    
}

@end
