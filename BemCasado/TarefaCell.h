//
//  TarefaCell.h
//  BemCasado
//
//  Created by Erick Vicente on 05/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TarefaCell : UITableViewCell{
    
    UILabel *nomeTarefaLabel;
    UILabel *prazoTarefaLabel;
    UIImageView *imagemStatus;
}

@property (nonatomic, retain) IBOutlet UILabel *nomeTarefaLabel;
@property (nonatomic, retain) IBOutlet UILabel *prazoTarefaLabel;
@property (nonatomic, retain) IBOutlet UIImageView *imagemStatus;

@end
