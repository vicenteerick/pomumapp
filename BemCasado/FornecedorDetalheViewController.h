//
//  FornecedorDetalheViewController.h
//  BemCasado
//
//  Created by Erick Vicente on 20/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Fornecedor.h"

@interface FornecedorDetalheViewController : UIViewController{
 
    Fornecedor *fornecedor;
    UITextField *nomeTextField;
    UITextField *enderecoTextField;
    UITextField *telefoneTextField;
    UITextField *emailTextField;
    UITextField *siteTextField;
    UITextField *contatoTextField;
    
}

@property (retain, nonatomic) Fornecedor *fornecedor;
@property (retain, nonatomic) IBOutlet UITextField *nomeTextField;
@property (retain, nonatomic) IBOutlet UITextField *enderecoTextField;
@property (retain, nonatomic) IBOutlet UITextField *telefoneTextField;
@property (retain, nonatomic) IBOutlet UITextField *emailTextField;
@property (retain, nonatomic) IBOutlet UITextField *siteTextField;
@property (retain, nonatomic) IBOutlet UITextField *contatoTextField;

- (IBAction)voltar:(id)sender;

@end
