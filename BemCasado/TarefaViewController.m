//
//  TarefaViewController.m
//  BemCasado
//
//  Created by Erick Vicente on 05/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "TarefaViewController.h"

@interface TarefaViewController ()

@end

@implementation TarefaViewController

@synthesize tabelaDeTarefas;
@synthesize listaDeTarefas;
@synthesize tarefaSelecionada;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self iniciarTabela];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self viewDidLoad];
    
}

-(void) iniciarTabela{
    
    self.listaDeTarefas = [self ordenar:[[ServicoTarefa alloc] listarPor:1]];
    
    [self.tabelaDeTarefas reloadData];
    
}

#pragma mark UITableView DataSource and Delegate

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.listaDeTarefas count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"TarefaCell";
    
    TarefaCell *cell = (TarefaCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TarefaCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Tarefa *tarefa = [self.listaDeTarefas objectAtIndex:indexPath.row];
    cell.nomeTarefaLabel.text = tarefa.getNome;
    if (tarefa.getPrazo != nil) {
        
        NSDateFormatter *formatarData = [[NSDateFormatter alloc] init];
        [formatarData setDateFormat:@"dd/MM/yyyy"];
        
        cell.prazoTarefaLabel.text = [[NSString alloc] initWithFormat:@"Prazo: %@", [formatarData stringFromDate:tarefa.getPrazo]];
        
    }else{
        cell.prazoTarefaLabel.text = @"";
    }
    
    if (tarefa.estaConcluida) {
        cell.imagemStatus.image = [UIImage imageNamed:@"concluido.png"];
    }else{
        cell.imagemStatus.image = [UIImage imageNamed:@"nao-concluido.png"];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
     //retornando o contato na posicao row
     Tarefa *tarefa = [self.listaDeTarefas objectAtIndex:indexPath.row];
    
    self.tarefaSelecionada = tarefa;
    
    [self performSegueWithIdentifier:@"TarefaDetalheSegue" sender:nil];     
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    UIBarButtonItem *button = (UIBarButtonItem *)sender;
    
    if (button != nil) {
        self.tarefaSelecionada = nil;
    }
    if ([segue.identifier isEqualToString:@"TarefaDetalheSegue"])
	{
        UINavigationController *navigationController = segue.destinationViewController;
		TarefaDetalheViewController *tarefaDetalheViewController = [[navigationController viewControllers] objectAtIndex:0];
		tarefaDetalheViewController.tarefa = self.tarefaSelecionada;
        [segue destinationViewController];
	}
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (NSMutableArray *)ordenar:(NSMutableArray *)lista{
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"prazo" ascending:YES];
    NSArray *sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];
    NSArray *listaOrdenadas = [lista sortedArrayUsingDescriptors:sortDescriptors];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"concluida" ascending:YES];
    sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];
    listaOrdenadas = [listaOrdenadas sortedArrayUsingDescriptors:sortDescriptors];
    
    return [listaOrdenadas mutableCopy];
}

@end
