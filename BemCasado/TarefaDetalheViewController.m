//
//  TarefaDetalheViewController.m
//  BemCasado
//
//  Created by Erick Vicente on 06/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "TarefaDetalheViewController.h"

@interface TarefaDetalheViewController ()

@end

@implementation TarefaDetalheViewController

@synthesize tarefa;
@synthesize nomeTextField;
@synthesize prazoTextField;
@synthesize valorTextField;
@synthesize concluidoSwitch;
@synthesize botaoItemBar;
@synthesize isAdicionar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self carregarDados];
}

- (void)carregarDados{
    
    if (tarefa != nil) {
        
        nomeTextField.text = tarefa.getNome;
        
        if (tarefa.getPrazo != nil) {
            
            NSDateFormatter *formatarData = [[NSDateFormatter alloc] init];
            [formatarData setDateFormat:@"dd/MM/yyyy"];
            
            prazoTextField.text = [formatarData stringFromDate:tarefa.getPrazo];
            
        }
        
        valorTextField.text = [[NSString alloc] initWithFormat:@"R%@", [self  numeroFormatado:[tarefa.getValor doubleValue]]];
        
        [concluidoSwitch setOn:tarefa.estaConcluida animated:YES];
        
        botaoItemBar.image = [UIImage imageNamed:@"edit-tarefa.png"];
        isAdicionar = NO;
        
    }else{
        
        nomeTextField.text = @"";
        prazoTextField.text = @"";
        valorTextField.text = @"";
        [concluidoSwitch setOn:NO animated:YES];
        
        botaoItemBar.image = [UIImage imageNamed:@"add-tarefa.png"];
        isAdicionar = YES;
    }

}

- (IBAction)adicionarOuEditar:(id)sender{
    
    Tarefa *novaTarefa = [[Tarefa alloc] init];
    [novaTarefa setNome:nomeTextField.text];
    
    NSDateFormatter *formatarData = [[NSDateFormatter alloc] init];
    [formatarData setDateFormat:@"dd/MM/yyyy"];
    NSDate *dataPrazo  = [formatarData dateFromString:prazoTextField.text];
    
    [novaTarefa setPrazo:dataPrazo];
    
    NSString *valor = [self formatarStringDecimal:valorTextField.text];
    
    if(![valor  isEqual: @""]){
        [novaTarefa setValor:[NSDecimalNumber decimalNumberWithString:valor]];
    }else{
        [novaTarefa setValor:[NSDecimalNumber decimalNumberWithString:@"0.0"]];
    }
    
    [novaTarefa setConcluida:concluidoSwitch.isOn];
    
    ServicoTarefa *servicoTarefa = [[ServicoTarefa alloc] init];
    
    if (isAdicionar) {
        
        tarefa = [servicoTarefa salvar:novaTarefa ePorCasamento:1];
        
    }else{
       
        [novaTarefa setID:tarefa.getID];
        [servicoTarefa atualizar:novaTarefa];
        tarefa = novaTarefa;
        
    }
    
    TarefaViewController *tarefaViewController = [[TarefaViewController alloc] init];
    HomeViewController *homeViewController = [[HomeViewController alloc] init];
    
    [tarefaViewController viewDidAppear:YES];
    [tarefaViewController loadView];
    [homeViewController viewDidAppear:YES];
    [homeViewController loadView];
    
    [self backgroundTouch:sender];
    
}

- (NSString *)formatarStringDecimal: (NSString *)valor{
    
    valor = [valor stringByReplacingOccurrencesOfString:@" " withString:@""];
    valor = [valor stringByReplacingOccurrencesOfString:@"R" withString:@""];
    valor = [valor stringByReplacingOccurrencesOfString:@"$" withString:@""];
    valor = [valor stringByReplacingOccurrencesOfString:@"." withString:@""];
    valor = [valor stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    return valor;
}

- (NSString *)numeroFormatado:(double)numero{
    
    NSNumberFormatter *formato = [[NSNumberFormatter alloc] init];
    [formato setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [formato setGroupingSeparator:groupingSeparator];
    [formato setGroupingSize:3];
    [formato setAlwaysShowsDecimalSeparator:NO];
    [formato setUsesGroupingSeparator:YES];
    
    NSString *stringNumero = [[formato stringFromNumber:[NSNumber numberWithFloat:numero]] stringByReplacingOccurrencesOfString:@"." withString:@";"];
    
    stringNumero = [stringNumero stringByReplacingOccurrencesOfString:@"," withString:@"."];
    stringNumero = [stringNumero stringByReplacingOccurrencesOfString:@";" withString:@","];
    
    return stringNumero;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)voltar:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)textFieldReturn:(id)sender
{
    [sender resignFirstResponder];
}

- (IBAction)backgroundTouch:(id)sender
{
    [nomeTextField resignFirstResponder];
    [prazoTextField resignFirstResponder];
    [valorTextField resignFirstResponder];
}

@end
