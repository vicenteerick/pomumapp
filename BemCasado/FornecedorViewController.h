//
//  FornecedorViewController.h
//  BemCasado
//
//  Created by Erick Vicente on 20/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Fornecedor.h"
#import "FornecedorCell.h"
#import "ServicoFornecedor.h"
#import "FornecedorDetalheViewController.h"

@interface FornecedorViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    
    UITableView *tabelaDeFornecedores;
    NSMutableArray *listaDeFornecedores;
    Fornecedor *fornecedorSelecionado;
    
}

@property (strong, nonatomic) IBOutlet UITableView *tabelaDeFornecedores;
@property (strong, nonatomic) NSMutableArray *listaDeFornecedores;
@property (strong, nonatomic) Fornecedor *fornecedorSelecionado;

- (IBAction)voltar:(id)sender;

@end
