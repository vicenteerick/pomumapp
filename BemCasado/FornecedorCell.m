//
//  FornecedorCell.m
//  BemCasado
//
//  Created by Erick Vicente on 19/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "FornecedorCell.h"

@implementation FornecedorCell

@synthesize nomeFornecedorLabel;
@synthesize emailFornecedorLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
