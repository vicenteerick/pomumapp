//
//  Casamento.m
//  BemCasado
//
//  Created by Erick Vicente on 24/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "Casamento.h"

@implementation Casamento

-(void) setID:(int)novoID{
    ID = novoID;
}
-(void) setNoivo:(NSString *)novoNoivo{
    noivo = novoNoivo;
}
-(void) setNoiva:(NSString *)novaNoiva{
    noiva = novaNoiva;
}
-(void) setData:(NSDate *)novaData{
    data = novaData;
}
-(void) setOrcamento:(NSDecimalNumber *)novoOrcamento{
    orcamento = novoOrcamento;
}
-(void) setConvidados:(NSMutableArray *)novosConvidados{
    convidados = novosConvidados;
}
-(void) setTarefas:(NSMutableArray *)novasTarefas{
    tarefas = novasTarefas;
}

-(int) getID{
    return ID;
}
-(NSString *) getNoivo{
    return noivo;
}
-(NSString *) getNoiva{
    return noiva;
}
-(NSDate *) getData{
    return data;
}
-(NSDecimalNumber *)getOrcamento{
    return orcamento;
}
-(NSMutableArray *) getConvidados{
    return convidados;
}
-(NSMutableArray *) getTarefas{
    return tarefas;
}

-(NSMutableDictionary *) paraNSMutableDictionary{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:[NSNumber numberWithInteger:self.getID]forKey:@"ID"];
    [dictionary setValue:self.getNoivo forKey:@"noivo"];
    [dictionary setValue:self.getNoiva forKey:@"noiva"];
    [dictionary setValue:self.getData forKey:@"data"];
    [dictionary setValue:self.getOrcamento forKey:@"orcamento"];
    [dictionary setValue:self.getTarefas forKey:@"tarefas"];
    NSMutableArray *listaDeConvidados = [[NSMutableArray alloc] init];
    
    for (Convidado *convidado in self.getConvidados) {
        [listaDeConvidados addObject:[convidado paraNSDictionary]];
    }
    
    [dictionary setObject:listaDeConvidados forKey:@"convidados"];
    
    NSMutableArray *listaDeTarefas = [[NSMutableArray alloc] init];
    
    for (Tarefa *tarefa in self.getTarefas) {
        [listaDeTarefas addObject:[tarefa paraNSDictionary]];
    }
    
    [dictionary setObject:listaDeTarefas forKey:@"tarefas"];
    
    return dictionary;
    
}

-(Casamento *)paraOjetoCasamentoDe:(NSDictionary *)dicionario{
    
    NSString *dataDicionario = [dicionario objectForKey:@"data"];
    
    NSDateFormatter *formatarData = [[NSDateFormatter alloc] init];
    [formatarData setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssz"];
    NSDate *dataCasamento  = [formatarData dateFromString:dataDicionario];
    
    [self setID: [[dicionario objectForKey:@"ID"] intValue]];
    [self setNoivo:[dicionario objectForKey:@"noivo"]];
    [self setNoiva:[dicionario objectForKey:@"noiva"]];
    [self setData:dataCasamento];
    [self setOrcamento:[NSDecimalNumber decimalNumberWithString:[dicionario objectForKey:@"orcamento"]]];
    
    return self;
     
}

@end
