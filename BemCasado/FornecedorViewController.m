//
//  FornecedorViewController.m
//  BemCasado
//
//  Created by Erick Vicente on 20/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "FornecedorViewController.h"

@interface FornecedorViewController ()

@end

@implementation FornecedorViewController

@synthesize tabelaDeFornecedores;
@synthesize listaDeFornecedores;
@synthesize fornecedorSelecionado;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self iniciarTabela];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) iniciarTabela{
    
    self.listaDeFornecedores = [self ordenar:[[ServicoFornecedor alloc] listar]];
    
}

#pragma mark UITableView DataSource and Delegate

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.listaDeFornecedores count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"FornecedorCell";
    
    FornecedorCell *cell = (FornecedorCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FornecedorCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Fornecedor *fornecedor = [self.listaDeFornecedores objectAtIndex:indexPath.row];
    
    cell.nomeFornecedorLabel.text = fornecedor.getNome;
    cell.emailFornecedorLabel.text = fornecedor.getEmail;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //retornando o contato na posicao row
    Fornecedor *fornecedor = [self.listaDeFornecedores objectAtIndex:indexPath.row];
    
    self.fornecedorSelecionado = fornecedor;
    
    [self performSegueWithIdentifier:@"FornecedorDetalheSegue" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    UIBarButtonItem *button = (UIBarButtonItem *)sender;
    
    if (button != nil) {
        self.fornecedorSelecionado = nil;
    }
    if ([segue.identifier isEqualToString:@"FornecedorDetalheSegue"])
	{
        UINavigationController *navigationController = segue.destinationViewController;
		FornecedorDetalheViewController *fornecedorDetalheViewController = [[navigationController viewControllers] objectAtIndex:0];
		fornecedorDetalheViewController.fornecedor = self.fornecedorSelecionado;
        [segue destinationViewController];
	}
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (IBAction)voltar:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (NSMutableArray *)ordenar:(NSMutableArray *)lista{
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nome" ascending:YES];
    NSArray *sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];
    NSArray *listaOrdenadas = [lista sortedArrayUsingDescriptors:sortDescriptors];
    
    return [listaOrdenadas mutableCopy];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
