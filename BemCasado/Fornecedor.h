//
//  Fornecedor.h
//  BemCasado
//
//  Created by Erick Vicente on 24/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fornecedor : NSObject{
    
    int ID;
    NSString *nome;
    NSString *endereco;
    NSString *telefone;
    NSString *email;
    NSString *site;
    NSString *contato;
}

-(void) setID:(int)novoID;
-(void) setNome:(NSString *)novoNome;
-(void) setEndereco:(NSString *)novoEndereco;
-(void) setTelefone:(NSString *)novaTelefone;
-(void) setEmail:(NSString *)novaEmail;
-(void) setSite:(NSString *)novaSite;
-(void) setContato:(NSString *)novaContato;

-(int) getID;
-(NSString *) getNome;
-(NSString *) getEndereco;
-(NSString *) getTelefone;
-(NSString *) getEmail;
-(NSString *) getSite;
-(NSString *) getContato;

-(NSMutableDictionary *) paraNSDictionary;
-(Fornecedor *)paraOjetoTarefaDe:(NSDictionary *)dicionario;

@end
