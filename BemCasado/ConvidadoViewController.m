//
//  ConvidadoViewController.m
//  BemCasado
//
//  Created by Erick Vicente on 04/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "ConvidadoViewController.h"

@interface ConvidadoViewController ()

@end

@implementation ConvidadoViewController

@synthesize tabelaDeConvidados;
@synthesize listaDeConvidados;
@synthesize convidadoSelecionado;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self iniciarTabela];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self viewDidLoad];
    
}

-(void) iniciarTabela{
    
    self.listaDeConvidados = [self ordenar:[[ServicoConvidado alloc] listarPor:1]];
    
    [self.tabelaDeConvidados reloadData];
    
}

#pragma mark UITableView DataSource and Delegate

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.listaDeConvidados count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"ConvidadoCell";
    
    ConvidadoCell *cell = (ConvidadoCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ConvidadoCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Convidado *convidado = [self.listaDeConvidados objectAtIndex:indexPath.row];
    cell.nomeConvidadoLabel.text = convidado.getNome;
    cell.convidadosAdicionaisLabel.text = [[NSString alloc] initWithFormat:@"Convites Adicionais: %d", convidado.getConvidadosAdicionados];
    
    if (convidado.estaConfirmado) {
        cell.imagemStatus.image = [UIImage imageNamed:@"confirmado.png"];
    }else{
       cell.imagemStatus.image = [UIImage imageNamed:@"duvida.png"];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    Convidado *convidado = [self.listaDeConvidados objectAtIndex:indexPath.row];
    
    self.convidadoSelecionado = convidado;
    
    [self performSegueWithIdentifier:@"ConvidadoDetalheSegue" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    UIBarButtonItem *button = (UIBarButtonItem *)sender;
    
    if (button != nil) {
        self.convidadoSelecionado = nil;
    }
    if ([segue.identifier isEqualToString:@"ConvidadoDetalheSegue"])
	{
        UINavigationController *navigationController = segue.destinationViewController;
		ConvidadoDetalheViewController *convidadoDetalheViewController = [[navigationController viewControllers] objectAtIndex:0];
		convidadoDetalheViewController.convidado = self.convidadoSelecionado;
        [segue destinationViewController];
	}
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (NSMutableArray *)ordenar:(NSMutableArray *)lista{
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nome" ascending:YES];
    NSArray *sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];
    NSArray *listaOrdenadas = [lista sortedArrayUsingDescriptors:sortDescriptors];
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"confirmado" ascending:YES];
    sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];
    listaOrdenadas = [listaOrdenadas sortedArrayUsingDescriptors:sortDescriptors];
    
    return [listaOrdenadas mutableCopy];
}

@end
