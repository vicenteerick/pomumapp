//
//  Convidado.h
//  BemCasado
//
//  Created by Erick Vicente on 24/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Convidado : NSObject{
    
    int ID;
    NSString *nome;
    NSString *telefone;
    NSString *email;
    int convidadosAdicionais;
    bool confirmado;
}

-(void) setID:(int)novoID;
-(void) setNome:(NSString *)novoNome;
-(void) setTelefone:(NSString *)novaTelefone;
-(void) setEmail:(NSString *)novaEmail;
-(void) setConvidadosAdicionais:(int)novoConvidadosAdicionais;
-(void) setConfirmado:(bool)estaConfirmado;

-(int) getID;
-(NSString *) getNome;
-(NSString *) getTelefone;
-(NSString *) getEmail;
-(int) getConvidadosAdicionados;
-(bool) estaConfirmado;

-(NSMutableDictionary *) paraNSDictionary;
-(Convidado *)paraOjetoConvidadoDe:(NSDictionary *)dicionario;

@end
