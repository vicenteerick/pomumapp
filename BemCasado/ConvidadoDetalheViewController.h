//
//  ConvidadoDetalheViewController.h
//  BemCasado
//
//  Created by Erick Vicente on 07/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Convidado.h"
#import "ServicoConvidado.h"
#import "ConvidadoViewController.h"
#import "HomeViewController.h"

@interface ConvidadoDetalheViewController : UIViewController{
    
    Convidado *convidado;
    UITextField *nomeTextField;
    UITextField *telefoneTextField;
    UITextField *emailTextField;
    UILabel *convidadosAdicionaisLabel;
    UISlider *convidadosAdicionaiSlider;
    UISwitch *confirmadoSwitch;
    UIBarButtonItem *botaoItemBar;
    bool isAdicionar;
    
}

@property (nonatomic, retain) Convidado *convidado;
@property (nonatomic, retain) IBOutlet UITextField *nomeTextField;
@property (nonatomic, retain) IBOutlet UITextField *telefoneTextField;
@property (nonatomic, retain) IBOutlet UITextField *emailTextField;
@property (nonatomic, retain) IBOutlet UILabel *convidadosAdicionaisLabel;
@property (nonatomic, retain) IBOutlet UISlider *convidadosAdicionaiSlider;
@property (nonatomic, retain) IBOutlet UISwitch *confirmadoSwitch;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *botaoItemBar;
@property (nonatomic) bool isAdicionar;

- (IBAction)voltar:(id)sender;
- (IBAction)adicionarOuEditar:(id)sender;
- (IBAction)slideValueChange:(id)sender;
- (IBAction)textFieldReturn:(id)sender;
- (IBAction)backgroundTouch:(id)sender;

@end
