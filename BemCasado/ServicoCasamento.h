//
//  ServicoCasamento.h
//  BemCasado
//
//  Created by Erick Vicente on 25/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Casamento.h"
#import "FabricaDeServico.h"

@interface ServicoCasamento : FabricaDeServico

-(Casamento *) buscar:(int) idCasamento;

@end
