//
//  FornecedorCell.h
//  BemCasado
//
//  Created by Erick Vicente on 19/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FornecedorCell : UITableViewCell{
    
    UILabel *nomeFornecedorLabel;
    UILabel *emailFornecedorLabel;
    
}

@property (nonatomic, retain) IBOutlet UILabel *nomeFornecedorLabel;
@property (nonatomic, retain) IBOutlet UILabel *emailFornecedorLabel;


@end
