//
//  ConvidadoCell.h
//  BemCasado
//
//  Created by Erick Vicente on 04/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConvidadoCell : UITableViewCell{
    
    UILabel *nomeConvidadoLabel;
    UILabel *convidadosAdicionaisLabel;
    UIImageView *imagemStatus;
}

@property (nonatomic, retain) IBOutlet UILabel *nomeConvidadoLabel;
@property (nonatomic, retain) IBOutlet UILabel *convidadosAdicionaisLabel;
@property (nonatomic, retain) IBOutlet UIImageView *imagemStatus;

@end
