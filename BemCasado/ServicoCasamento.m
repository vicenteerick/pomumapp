//
//  ServicoCasamento.m
//  BemCasado
//
//  Created by Erick Vicente on 25/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "ServicoCasamento.h"

@implementation ServicoCasamento

NSString *const CAMINHODECASAMENTO = @"casamento";

-(Casamento *)buscar:(int)idCasamento{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/buscar/%d", CAMINHODECASAMENTO, idCasamento];
    
    Casamento *casamento = [[Casamento alloc] paraOjetoCasamentoDe:[self getPor:caminho]];
    
    return casamento;
}

@end
