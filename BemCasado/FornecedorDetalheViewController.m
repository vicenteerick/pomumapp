//
//  FornecedorDetalheViewController.m
//  BemCasado
//
//  Created by Erick Vicente on 20/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "FornecedorDetalheViewController.h"

@interface FornecedorDetalheViewController ()

@end

@implementation FornecedorDetalheViewController

@synthesize fornecedor;
@synthesize nomeTextField;
@synthesize enderecoTextField;
@synthesize telefoneTextField;
@synthesize emailTextField;
@synthesize siteTextField;
@synthesize contatoTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)voltar:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
