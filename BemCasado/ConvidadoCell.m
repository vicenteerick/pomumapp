//
//  ConvidadoCell.m
//  BemCasado
//
//  Created by Erick Vicente on 04/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "ConvidadoCell.h"

@implementation ConvidadoCell

@synthesize nomeConvidadoLabel;
@synthesize convidadosAdicionaisLabel;
@synthesize imagemStatus;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
