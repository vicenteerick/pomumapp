//
//  Fornecedor.m
//  BemCasado
//
//  Created by Erick Vicente on 24/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "Fornecedor.h"

@implementation Fornecedor

-(void) setID:(int)novoID{
    ID = novoID;
}
-(void) setNome:(NSString *)novoNome{
    nome = novoNome;
}
-(void) setEndereco:(NSString *)novoEndereco{
    endereco = novoEndereco;
}
-(void) setTelefone:(NSString *)novaTelefone{
    telefone = novaTelefone;
}
-(void) setEmail:(NSString *)novaEmail{
    email = novaEmail;
}
-(void) setSite:(NSString *)novaSite{
    site = novaSite;
}
-(void) setContato:(NSString *)novaContato{
    contato = novaContato;
}

-(int) getID{
    return ID;
}
-(NSString *) getNome{
    return nome;
}
-(NSString *) getEndereco{
    return endereco;
}
-(NSString *) getTelefone{
    return telefone;
}
-(NSString *) getEmail{
    return email;
}
-(NSString *) getSite{
    return site;
}
-(NSString *) getContato{
    return contato;
}

-(NSMutableDictionary *) paraNSDictionary{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:[NSNumber numberWithInt:self.getID] forKey:@"ID"];
    [dictionary setValue:self.getNome forKey:@"nome"];
    [dictionary setValue:self.getEndereco forKey:@"endereco"];
    [dictionary setValue:self.getTelefone forKey:@"telefone"];
    [dictionary setValue:self.getEmail forKey:@"email"];
    [dictionary setValue:self.getSite forKey:@"site"];
    [dictionary setValue:self.getContato forKey:@"contato"];
    
    return dictionary;
    
}

-(Fornecedor *)paraOjetoTarefaDe:(NSDictionary *)dicionario{
    
    [self setID: [[dicionario objectForKey:@"ID"] intValue]];
    [self setNome:[dicionario objectForKey:@"nome"]];
    [self setEndereco:[dicionario objectForKey:@"endereco"]];
    [self setTelefone:[dicionario objectForKey:@"telefone"]];
    [self setEmail:[dicionario objectForKey:@"email"]];
    [self setSite:[dicionario objectForKey:@"site"]];
    [self setContato:[dicionario objectForKey:@"contato"]];
    
    return self;
    
}

@end
