//
//  FabricaDeServico.m
//  BemCasado
//
//  Created by Erick Vicente on 01/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "FabricaDeServico.h"

@implementation FabricaDeServico

NSString *const CAMINHOURL = @"http://localhost:8080/BemCasado/";

-(NSDictionary *)getPor:(NSString *)caminho{
    
    // URL do serviço
    NSString *stringUrl = [CAMINHOURL stringByAppendingString:caminho];
    NSURL *url = [NSURL URLWithString:stringUrl];
    
    // Submetendo a requisição sem o NSMutableURLRequest, assim funciona com GET.
    NSError *error;
    NSData *resultado = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:&error];
    
    // Verificando a ocorrência de erros HTTP.
    if(error){
        NSLog(@"Erro HTTP: %@", error.description);
        return nil;
    }
    
    NSDictionary *dicionario = [NSJSONSerialization JSONObjectWithData:resultado options:kNilOptions error:nil];
    
    if (dicionario == nil) {
        NSString *resquisicaoSimples = [[NSString alloc] initWithData:resultado encoding:4];
        
        dicionario = [[NSDictionary alloc] initWithObjectsAndKeys:resquisicaoSimples, @"dado", nil];
    }
    
    // Acessando o elemento da estrutura retornada pelo serviço.
    return dicionario;
    
}

-(NSDictionary *)postPorCaminho:(NSString *)caminho eDicionario:(NSDictionary *)objetoDicionario{
    
    // URL do serviço de monitoramento no ambiente de testes (sandbox).
    NSURL *url = [NSURL URLWithString:[CAMINHOURL stringByAppendingString:caminho]];
    
    // Criando a requisição com o método POST.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSError *error;
    
    NSData *dado = [NSJSONSerialization dataWithJSONObject:objetoDicionario options:0 error:&error];
    
    // Usando o POST devemos passando os parâmetros via body exigidos pelo serviço.
    [request setHTTPBody:dado];
    
    // Verificando a ocorrência de erros de serialização.
    if(error){
        NSLog(@"Erro ao serializar objeto: %@", error.description);
        return NO;
    }
    
    // Serviço exige que os parâmetros sejam passados via FORM.
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // Submetendo a requisição e obtendo o resultado.
    NSData *resultado = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    
    // Verificando a ocorrência de erros HTTP.
    if(error){
        NSLog(@"Erro HTTP: %@", error.description);
        return NO;
    }
    
    NSDictionary *dicionario = [NSJSONSerialization JSONObjectWithData:resultado options:kNilOptions error:&error];
    
    if (dicionario == nil) {
        NSString *resquisicaoSimples = [[NSString alloc] initWithData:resultado encoding:4];
        
        dicionario = [[NSDictionary alloc] initWithObjectsAndKeys:resquisicaoSimples, @"dado", nil];
    }
    
    // Acessando o elemento da estrutura retornada pelo serviço.
    return dicionario;
    
}

-(NSDictionary *) putPorCaminho:(NSString *)caminho eDicionario:(NSDictionary *)objetoDicionario{
    
    // URL do serviço de monitoramento no ambiente de testes (sandbox).
    NSURL *url = [NSURL URLWithString:[CAMINHOURL stringByAppendingString:caminho]];
    
    // Criando a requisição com o método POST.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"PUT"];
    
    NSError *error;
    
    NSData *dado = [NSJSONSerialization dataWithJSONObject:objetoDicionario options:0 error:&error];
    
    // Usando o POST devemos passando os parâmetros via body exigidos pelo serviço.
    [request setHTTPBody:dado];
    
    // Verificando a ocorrência de erros de serialização.
    if(error){
        NSLog(@"Erro ao serializar objeto: %@", error.description);
        return NO;
    }
    
    // Serviço exige que os parâmetros sejam passados via FORM.
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // Submetendo a requisição e obtendo o resultado.
    NSData *resultado = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    
    // Verificando a ocorrência de erros HTTP.
    if(error){
        NSLog(@"Erro HTTP: %@", error.description);
        return NO;
    }
    
    NSDictionary *dicionario = [NSJSONSerialization JSONObjectWithData:resultado options:kNilOptions error:&error];
    
    if (dicionario == nil) {
        NSString *resquisicaoSimples = [[NSString alloc] initWithData:resultado encoding:4];
        
        dicionario = [[NSDictionary alloc] initWithObjectsAndKeys:resquisicaoSimples, @"dado", nil];
    }
    
    return dicionario;
    
}

@end
