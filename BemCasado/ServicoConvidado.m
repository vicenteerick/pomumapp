//
//  ServicoConvidado.m
//  BemCasado
//
//  Created by Erick Vicente on 03/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "ServicoConvidado.h"

@implementation ServicoConvidado

NSString *const CAMINHODECONVIDADO = @"convidado";

-(NSMutableArray *) listarPor:(int)idCasamento{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/listarPor/%d", CAMINHODECONVIDADO, idCasamento];
    
    NSDictionary *dicionario = [self getPor:caminho];
    
    NSMutableArray *convidadosJSON = [dicionario objectForKey:@"convidado"];
    
    NSMutableArray *convidados = [[NSMutableArray alloc] init];
    
    for (NSDictionary *convidadoJSON in convidadosJSON ) {
        
        Convidado *convidado = [[Convidado alloc] paraOjetoConvidadoDe:convidadoJSON];
        
        [convidados addObject:convidado];
        
    }
    
    return convidados;
}

-(int) totalPor:(bool)estaConfirmado ePorCasamento:(int)idCasamento{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/totalPor/%@/%d", CAMINHODECONVIDADO, (estaConfirmado ? @"true" : @"false"), idCasamento];
    
    NSString *resposta = [[self getPor:caminho] objectForKey:@"dado"];
    
    return [resposta intValue];
}

-(Convidado *) salvar:(Convidado *)convidado ePorCasamento:(int)idCasamento{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/salvar/%d", CAMINHODECONVIDADO, idCasamento];
    
    NSDictionary *dicionario = [[self postPorCaminho:caminho eDicionario:[convidado paraNSDictionary]] objectForKey:@"dado"];
    
    convidado = [convidado paraOjetoConvidadoDe:dicionario];
    
    return convidado;
}

-(bool) atualizar:(Convidado *)convidado{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/atualizar", CAMINHODECONVIDADO];
    
    bool resposta = [[[self putPorCaminho:caminho eDicionario:[convidado paraNSDictionary]] objectForKey:@"dado"] boolValue];
    
    return resposta;
    
}

@end
