//
//  HomeViewController.m
//  BemCasado
//
//  Created by Erick Vicente on 11/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize myImageView;
@synthesize myImage;
@synthesize adicionarImagemButton;
@synthesize casalLabel;
@synthesize diasParaCasamentoLabel;
@synthesize convidadosLabel;
@synthesize tarefasPendentesLabel;
@synthesize totalConvidadosLabel;
@synthesize totalTarefasLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self carregarDados];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(carregarDados) name:@"ReloadNotification" object:nil];
}

-(void)carregarDados{
    
    myImageFrame = myImageView.frame;
    
    Casamento *casamento = [[ServicoCasamento alloc] buscar:1];
    
    NSString *casal = [[NSString alloc] initWithFormat:@"%@ e %@", casamento.getNoivo, casamento.getNoiva];
    casalLabel.text = [casal uppercaseString];
    
    NSDate *diaDoCasamento = casamento.getData;
    int diasParaCasamento = (int)[diaDoCasamento timeIntervalSinceNow] / 86400;
    diasParaCasamentoLabel.text = [[NSString alloc] initWithFormat:@"%d", diasParaCasamento];
    
    ServicoConvidado *servicoConvidado = [[ServicoConvidado alloc] init];
    int confirmados = [servicoConvidado totalPor:TRUE ePorCasamento:1];
    int naoConfirmados = [servicoConvidado totalPor:FALSE ePorCasamento:1];
    int totalConvidados = confirmados + naoConfirmados;
    
    //TESTE
    Convidado *convidado1 = [[Convidado alloc] init];
    [convidado1 setNome:@"Alberto"];
    [convidado1 setTelefone:@"21986438958"];
    [convidado1 setEmail:@"alberto@email.com"];
    [convidado1 setConvidadosAdicionais:2];
    [convidado1 setConfirmado:YES];
    
    //[servicoConvidado salvar:convidado1 ePorCasamento:1];
    
    convidadosLabel.text = [[NSString alloc] initWithFormat:@"%d", confirmados];
    totalConvidadosLabel.text = [[NSString alloc] initWithFormat:@"%d", totalConvidados];
    
    ServicoTarefa *servicoTarefa = [[ServicoTarefa alloc] init];
    int concluidos = [servicoTarefa totalPor:TRUE ePorCasamento:1];
    int naoConcluidos = [servicoTarefa totalPor:FALSE ePorCasamento:1];
    int totalTarefas = concluidos + naoConcluidos;
    
    tarefasPendentesLabel.text = [[NSString alloc] initWithFormat:@"%d", concluidos];
    totalTarefasLabel.text = [[NSString alloc] initWithFormat:@"%d", totalTarefas];
    
    double valorTotalTarefas = [servicoTarefa valorTotalPor:1];
    
    double valorGasto = (valorTotalTarefas * 100) / [casamento.getOrcamento doubleValue];
    
    NSString *texto = [[NSString alloc] initWithFormat:@"R%@ %@ de R%@", [self numeroFormatado:valorTotalTarefas], valorTotalTarefas == 1.0f ? @"gasto" : @"gastos",  [self numeroFormatado:[casamento.getOrcamento doubleValue]]];
    
    [self addProgressBarInFrame:CGRectMake(19.f, 430.f, 280.f, 30.f) withProgress:valorGasto/100 andText:texto];
    
}

-(void)addProgressBarInFrame:(CGRect)frame withProgress:(CGFloat)progress andText:(NSString *)text
{
    float widthOfJaggedBit = 4.0f;
    
    UIImage *imagemA;
    
    if (progress < .5f) {
        imagemA = [[UIImage imageNamed:@"progress.png"] stretchableImageWithLeftCapWidth:widthOfJaggedBit topCapHeight:0.0f];
    }else if (progress >= .5f && progress < .8f){
        imagemA = [[UIImage imageNamed:@"progress_atencao.png"] stretchableImageWithLeftCapWidth:widthOfJaggedBit topCapHeight:0.0f];
    }else{
        imagemA = [[UIImage imageNamed:@"progress_perigo.png"] stretchableImageWithLeftCapWidth:widthOfJaggedBit topCapHeight:0.0f];
    }
    
    UIImage *imagemB = [[UIImage imageNamed:@"track.png"] stretchableImageWithLeftCapWidth:widthOfJaggedBit topCapHeight:0.0f];
    
    UIView * progressBar = [[UIView alloc] initWithFrame:frame];
    progressBar.backgroundColor = [UIColor whiteColor];
    
    UIImageView * imageViewA = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width*progress, frame.size.height)];
    UIImageView * imageViewB = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width*progress, 0.f, frame.size.width - (frame.size.width*progress), frame.size.height)];
    
    imageViewA.image = imagemA;
    imageViewB.image = imagemB;
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = text;
    label.textAlignment = 1;
    label.textColor = [[UIColor alloc] initWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
    label.font = [UIFont fontWithName:@"GeezaPro-Bold" size:13.0f];
    
    [self.view addSubview:progressBar];
    [progressBar addSubview:imageViewA];
    [progressBar addSubview:imageViewB];
    [self.view addSubview:label];
    
}

-(NSString *)numeroFormatado:(double)numero{
    
    NSNumberFormatter *formato = [[NSNumberFormatter alloc] init];
    [formato setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [formato setGroupingSeparator:groupingSeparator];
    [formato setGroupingSize:3];
    [formato setAlwaysShowsDecimalSeparator:NO];
    [formato setUsesGroupingSeparator:YES];
    
    NSString *stringNumero = [[formato stringFromNumber:[NSNumber numberWithFloat:numero]] stringByReplacingOccurrencesOfString:@"." withString:@";"];
    
    stringNumero = [stringNumero stringByReplacingOccurrencesOfString:@"," withString:@"."];
    stringNumero = [stringNumero stringByReplacingOccurrencesOfString:@";" withString:@","];
    
    return stringNumero;
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self viewDidLoad];
    
    myImageView.image = myImage;
    myImageView.hidden = NO;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) escolherFotoDoAlbum
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    NSArray *arrayMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
    
    if([arrayMediaTypes count] > 0 && [UIImagePickerController isSourceTypeAvailable:sourceType]){
        
        UIImagePickerController *imagePickeController = [[UIImagePickerController alloc] init];
        
        imagePickeController.sourceType = sourceType;
        imagePickeController.allowsEditing = YES;
        imagePickeController.mediaTypes = arrayMediaTypes;
        imagePickeController.delegate = self;
        
        [self presentViewController:imagePickeController animated:YES completion:nil];
        
    }else{
        
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                          message:@"Dispositivo não encontrado."
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [myAlert show];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
        
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
    UIImage *adjustImage = [self EscalaECortePorTamanho:myImageFrame.size imagemEscolhida:chosenImage];
    //chosenImage:chosenImage resizeToSize:myImageFrame.size thenCropWithRect:myImageFrame];
    
    self.myImage = adjustImage;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (UIImage*)EscalaECortePorTamanho:(CGSize)tamanhoEscolhido imagemEscolhida:(UIImage *)imagem
{
    UIImage *sourceImage = imagem;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = tamanhoEscolhido.width;
    CGFloat targetHeight = tamanhoEscolhido.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, tamanhoEscolhido) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
        {
            scaleFactor = widthFactor; // scale to fit height
        }
        else
        {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
        {
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
        }
    }
    
    UIGraphicsBeginImageContext(tamanhoEscolhido); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil)
    {
        NSLog(@"could not scale image");
    }
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    if (operation == UINavigationControllerOperationPush) {
        return nil;//self.animator;
    }
    
    return nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
