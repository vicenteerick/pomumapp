//
//  Tarefa.h
//  BemCasado
//
//  Created by Erick Vicente on 25/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Fornecedor.h"

@interface Tarefa : NSObject{
    
    int ID;
    NSString *nome;
    NSDate *prazo;
    NSDecimalNumber *valor;
    bool concluida;
    NSMutableArray *fornecedores;
    
}

-(void) setID:(int)novoID;
-(void) setNome:(NSString *)novoNome;
-(void) setPrazo:(NSDate *)novoPrazo;
-(void) setValor:(NSDecimalNumber *)novoValor;
-(void) setConcluida:(bool)foiConcluida;
-(void) setFornecedores:(NSMutableArray *)novosFornecedores;

-(int) getID;
-(NSString *) getNome;
-(NSDate *) getPrazo;
-(NSDecimalNumber *) getValor;
-(bool) estaConcluida;
-(NSMutableArray *) getFornecedores;

-(NSMutableDictionary *) paraNSDictionary;
-(Tarefa *)paraOjetoTarefaDe:(NSDictionary *)dicionario;

@end
