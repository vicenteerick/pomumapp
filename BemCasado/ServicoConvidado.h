//
//  ServicoConvidado.h
//  BemCasado
//
//  Created by Erick Vicente on 03/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FabricaDeServico.h"
#import "Convidado.h"

@interface ServicoConvidado : FabricaDeServico

-(NSMutableArray *) listarPor:(int)idCasamento;
-(int) totalPor:(bool)estaConfirmado ePorCasamento:(int)idCasamento;
-(Convidado *) salvar:(Convidado *)convidado ePorCasamento:(int)idCasamento;
-(bool) atualizar:(Convidado *)convidado;

@end
