//
//  ConvidadoDetalheViewController.m
//  BemCasado
//
//  Created by Erick Vicente on 07/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "ConvidadoDetalheViewController.h"

@interface ConvidadoDetalheViewController ()

@end

@implementation ConvidadoDetalheViewController

@synthesize convidado;
@synthesize nomeTextField;
@synthesize telefoneTextField;
@synthesize emailTextField;
@synthesize convidadosAdicionaisLabel;
@synthesize convidadosAdicionaiSlider;
@synthesize confirmadoSwitch;
@synthesize botaoItemBar;
@synthesize isAdicionar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self carregarDados];
}

- (void)carregarDados{
    
    convidadosAdicionaisLabel.text = [[NSString alloc] initWithFormat:@"%f", [convidadosAdicionaiSlider value]];
    
    if(convidado != nil){
        
        nomeTextField.text = convidado.getNome;
        telefoneTextField.text = convidado.getTelefone;
        emailTextField.text = convidado.getEmail;
        convidadosAdicionaisLabel.text = [[NSString alloc] initWithFormat:@"%d", convidado.getConvidadosAdicionados];
        [convidadosAdicionaiSlider setValue:(float)convidado.getConvidadosAdicionados];
        [confirmadoSwitch setOn:convidado.estaConfirmado animated:YES];
        
        botaoItemBar.image = [UIImage imageNamed:@"edit-convidado.png"];
        isAdicionar = NO;
        
    }else{
        
        nomeTextField.text = @"";
        telefoneTextField.text = @"";
        emailTextField.text = @"";
        convidadosAdicionaisLabel.text = @"0";
        [convidadosAdicionaiSlider setValue:0.0f];
        [confirmadoSwitch setOn:NO animated:YES];
         
        botaoItemBar.image = [UIImage imageNamed:@"add-32.png"];
        isAdicionar = YES;
        
    }

}

- (IBAction)adicionarOuEditar:(id)sender{
    
    Convidado *novoConvidado = [[Convidado alloc] init];
    [novoConvidado setNome:nomeTextField.text];
    [novoConvidado setTelefone:telefoneTextField.text];
    [novoConvidado setEmail:emailTextField.text];
    [novoConvidado setConvidadosAdicionais: (int)[convidadosAdicionaiSlider value]];
    [novoConvidado setConfirmado:confirmadoSwitch.isOn];
    
    ServicoConvidado *servicoConvidado = [[ServicoConvidado alloc] init];
    
    if (isAdicionar) {
        
        convidado = [servicoConvidado salvar:novoConvidado ePorCasamento:1];
        
    }else{
        
        [novoConvidado setID:convidado.getID];
        [servicoConvidado atualizar:novoConvidado];
        convidado = novoConvidado;
    }
    
    ConvidadoViewController *convidadoViewController = [[ConvidadoViewController alloc] init];
    HomeViewController *homeViewController = [[HomeViewController alloc] init];
    
    [convidadoViewController viewDidAppear:YES];
    [convidadoViewController loadView];
    [homeViewController viewDidAppear:YES];
    [homeViewController loadView];
    
    
    [self backgroundTouch:sender];
    
}

- (IBAction)slideValueChange:(id)sender{
    
    convidadosAdicionaisLabel.text = [[NSString alloc] initWithFormat:@"%d", (int)convidadosAdicionaiSlider.value];
    
    [self backgroundTouch:sender];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)voltar:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)textFieldReturn:(id)sender
{
    [sender resignFirstResponder];
}

- (IBAction)backgroundTouch:(id)sender
{
    [nomeTextField resignFirstResponder];
    [telefoneTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
}

@end
