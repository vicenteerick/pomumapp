//
//  TarefaDetalheViewController.h
//  BemCasado
//
//  Created by Erick Vicente on 06/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tarefa.h"
#import "ServicoTarefa.h"
#import "TarefaViewController.h"
#import "HomeViewController.h"

@interface TarefaDetalheViewController : UIViewController{
    
    Tarefa *tarefa;
    UITextField *nomeTextField;
    UITextField *prazoTextField;
    UITextField *valorTextField;
    UISwitch *concluidoSwitch;
    UIBarButtonItem *botaoItemBar;
    bool isAdicionar;
    
}

@property (retain, nonatomic) Tarefa *tarefa;
@property (retain, nonatomic) IBOutlet UITextField *nomeTextField;
@property (retain, nonatomic) IBOutlet UITextField *prazoTextField;
@property (retain, nonatomic) IBOutlet UITextField *valorTextField;
@property (retain, nonatomic) IBOutlet UISwitch *concluidoSwitch;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *botaoItemBar;
@property (nonatomic) bool isAdicionar;

- (IBAction) voltar: (id)sender;
- (IBAction)adicionarOuEditar:(id)sender;
- (IBAction)textFieldReturn:(id)sender;
- (IBAction)backgroundTouch:(id)sender;

@end
