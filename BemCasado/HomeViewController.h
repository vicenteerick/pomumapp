//
//  HomeViewController.h
//  BemCasado
//
//  Created by Erick Vicente on 11/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ServicoCasamento.h"
#import "ServicoConvidado.h"
#import "ServicoTarefa.h"
#import "Casamento.h"

@interface HomeViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIImageView *myImageView;
    UIImage *myImage;
    CGRect myImageFrame;
    UIButton *adicionarImagemButton;
    UILabel *casalLabel;
    UILabel *diasParaCasamentoLabel;
    UILabel *convidadosLabel;
    UILabel *totalConvidadosLabel;
    UILabel *tarefasPendentesLabel;
    UILabel *totalTarefasLabel;
}

@property (nonatomic, retain) IBOutlet UIImageView *myImageView;
@property (nonatomic, retain) IBOutlet UIButton *adicionarImagemButton;
@property (nonatomic, retain) UIImage *myImage;
@property (nonatomic, retain) IBOutlet UILabel *casalLabel;
@property (nonatomic, retain) IBOutlet UILabel *diasParaCasamentoLabel;
@property (nonatomic, retain) IBOutlet UILabel *convidadosLabel;
@property (nonatomic, retain) IBOutlet UILabel *tarefasPendentesLabel;
@property (nonatomic, retain) IBOutlet UILabel *totalConvidadosLabel;
@property (nonatomic, retain) IBOutlet UILabel *totalTarefasLabel;

- (IBAction)escolherFotoDoAlbum;
- (void) carregarDados;

@end
