//
//  main.m
//  BemCasado
//
//  Created by Erick Vicente on 09/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
