//
//  ServicoFornecedor.m
//  BemCasado
//
//  Created by Erick Vicente on 20/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "ServicoFornecedor.h"

@implementation ServicoFornecedor

NSString *const CAMINHODEFORNECEDOR = @"fornecedor";

-(NSMutableArray *)listar{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/listar", CAMINHODEFORNECEDOR];
    
    NSDictionary *dicionario = [self getPor:caminho];
    
    NSMutableArray *fornecedoresJSON = [dicionario objectForKey:@"fornecedor"];
    
    NSMutableArray *fornecedores = [[NSMutableArray alloc] init];
    
    if ([fornecedoresJSON count] == 1) {
        [fornecedores addObject:fornecedoresJSON];
    }else{
    
        for (NSDictionary *fornecedorJSON in fornecedoresJSON ) {
            
            Fornecedor *fornecedor = [[Fornecedor alloc] paraOjetoTarefaDe:fornecedorJSON];
            
            [fornecedores addObject:fornecedor];
            
        }
        
    }
    return fornecedores;
    
}

-(Fornecedor *) salvar:(Fornecedor *)fornecedor{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/salvar", CAMINHODEFORNECEDOR];
    
    NSDictionary *dicionario = [[self postPorCaminho:caminho eDicionario:[fornecedor paraNSDictionary]] objectForKey:@"dado"];
    
    fornecedor = [fornecedor paraOjetoTarefaDe:dicionario];
    
    return fornecedor;
}

-(bool) atualizar:(Fornecedor *)fornecedor{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/atualizar", CAMINHODEFORNECEDOR];
    
    bool resposta = [[[self putPorCaminho:caminho eDicionario:[fornecedor paraNSDictionary]] objectForKey:@"dado"] boolValue];
    
    return resposta;
    
}


@end
