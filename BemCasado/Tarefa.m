//
//  Tarefa.m
//  BemCasado
//
//  Created by Erick Vicente on 25/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "Tarefa.h"

@implementation Tarefa

-(void) setID:(int)novoID{
    ID = novoID;
}
-(void) setNome:(NSString *)novoNome{
    nome = novoNome;
}
-(void) setPrazo:(NSDate *)novoPrazo{
    prazo = novoPrazo;
}
-(void) setValor:(NSDecimalNumber *)novoValor{
    valor = novoValor;
}
-(void) setConcluida:(bool)foiConcluida{
    concluida = foiConcluida;
}
-(void) setFornecedores:(NSMutableArray *)novosFornecedores{
    fornecedores = novosFornecedores;
}

-(int) getID{
    return ID;
}
-(NSString *) getNome{
    return nome;
}
-(NSDate *) getPrazo{
    return prazo;
}
-(NSDecimalNumber *) getValor{
    return valor;
}
-(bool) estaConcluida{
    return concluida;
}
-(NSMutableArray *) getFornecedores{
    return fornecedores;
}

-(NSMutableDictionary *) paraNSDictionary{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    if(self.getID > 0){
        [dictionary setValue:[NSNumber numberWithInt:self.getID] forKey:@"ID"];
    }

    [dictionary setValue:self.getNome forKey:@"nome"];
    
    NSDateFormatter *formatarData = [[NSDateFormatter alloc] init];
    [formatarData setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSString *dataPrazo = [formatarData stringFromDate:self.getPrazo];
    
    [dictionary setValue:dataPrazo forKey:@"prazo"];
    
    if (self.getValor != nil) {
        [dictionary setValue:self.getValor forKey:@"valor"];
    }
    
    [dictionary setValue:[NSNumber numberWithBool:self.estaConcluida] forKey:@"concluida"];
    
    NSMutableArray *listaDeFornecedores = [[NSMutableArray alloc] init];
    
    for (Fornecedor *fornecedor in self.getFornecedores) {
        [listaDeFornecedores addObject:[fornecedor paraNSDictionary]];
    }
    
    [dictionary setObject:listaDeFornecedores forKey:@"fornecedores"];
    
    return dictionary;
    
}

-(Tarefa *)paraOjetoTarefaDe:(NSDictionary *)dicionario{
    
    NSString *prazoDicionario = [dicionario objectForKey:@"prazo"];
    
    NSDateFormatter *formatarData = [[NSDateFormatter alloc] init];
    [formatarData setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssz"];
    NSDate *dataPrazo  = [formatarData dateFromString:prazoDicionario];
    
    [self setID: [[dicionario objectForKey:@"ID"] intValue]];
    [self setNome:[dicionario objectForKey:@"nome"]];
    [self setPrazo:dataPrazo];
    [self setValor:[NSDecimalNumber decimalNumberWithString:[dicionario objectForKey:@"valor"]]];
    [self setConcluida:[[dicionario objectForKey:@"concluida"] boolValue]];
    
    return self;
    
}

@end
