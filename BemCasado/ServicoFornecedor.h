//
//  ServicoFornecedor.h
//  BemCasado
//
//  Created by Erick Vicente on 20/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "FabricaDeServico.h"
#import "Fornecedor.h"

@interface ServicoFornecedor : FabricaDeServico

-(NSMutableArray *)listar;
-(Fornecedor *) salvar:(Fornecedor *)fornecedor;
-(bool) atualizar:(Fornecedor *)fornecedor;

@end
