//
//  AppDelegate.h
//  BemCasado
//
//  Created by Erick Vicente on 09/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TarefaViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) TarefaViewController *tarefaViewController;

@end
