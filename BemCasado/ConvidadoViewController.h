//
//  ConvidadoViewController.h
//  BemCasado
//
//  Created by Erick Vicente on 04/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Convidado.h"
#import "ConvidadoCell.h"
#import "ServicoConvidado.h"
#import "ConvidadoDetalheViewController.h"


@interface ConvidadoViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    
    UITableView *tabelaDeConvidados;
    NSMutableArray *listaDeConvidados;
    Convidado *convidadoSelecionado;
    
}

@property (strong, nonatomic) IBOutlet UITableView *tabelaDeConvidados;
@property (strong, nonatomic) NSMutableArray *listaDeConvidados;
@property (strong, nonatomic) Convidado *convidadoSelecionado;

@end
