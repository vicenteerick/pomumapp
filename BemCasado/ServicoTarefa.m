//
//  ServicoTarefa.m
//  BemCasado
//
//  Created by Erick Vicente on 04/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import "ServicoTarefa.h"

@implementation ServicoTarefa

NSString *const CAMINHODETAREFA = @"tarefa";

-(NSMutableArray *)listarPor:(int)idCasamento{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/listarPor/%d", CAMINHODETAREFA, idCasamento];
    
    NSDictionary *dicionario = [self getPor:caminho];
    
    NSMutableArray *tarefasJSON = [dicionario objectForKey:@"tarefa"];
    
    NSMutableArray *tarefas = [[NSMutableArray alloc] init];
    
    for (NSDictionary *tarefaJSON in tarefasJSON ) {
        
        Tarefa *tarefa = [[Tarefa alloc] paraOjetoTarefaDe:tarefaJSON];
        
        [tarefas addObject:tarefa];
        
    }
    
    return tarefas;
    
}

-(int)totalPor:(bool)estaConfirmado ePorCasamento:(int)idCasamento{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/totalPor/%@/%d", CAMINHODETAREFA, (estaConfirmado ? @"true" : @"false"), idCasamento];
    
    NSString *resposta = [[self getPor:caminho] objectForKey:@"dado"];
    
    return [resposta intValue];
}

-(double)valorTotalPor:(int)idCasamento{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/valorTotalPor/%d", CAMINHODETAREFA, idCasamento];
    
    NSString *resposta = [[self getPor:caminho] objectForKey:@"dado"];
    
    return [resposta doubleValue];
}

-(Tarefa *) salvar:(Tarefa *)tarefa ePorCasamento:(int)idCasamento{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/salvar/%d", CAMINHODETAREFA, idCasamento];
    
    NSDictionary *dicionario = [[self postPorCaminho:caminho eDicionario:[tarefa paraNSDictionary]] objectForKey:@"dado"];
    
    tarefa = [tarefa paraOjetoTarefaDe:dicionario];
    
    return tarefa;
}

-(bool) atualizar:(Tarefa *)tarefa{
    
    NSString *caminho = [[NSString alloc] initWithFormat:@"%@/atualizar", CAMINHODETAREFA];
    
    bool resposta = [[[self putPorCaminho:caminho eDicionario:[tarefa paraNSDictionary]] objectForKey:@"dado"] boolValue];
    
    return resposta;
    
}

@end
