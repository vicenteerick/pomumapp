//
//  Casamento.h
//  BemCasado
//
//  Created by Erick Vicente on 24/04/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Convidado.h"
#import "Tarefa.h"

@interface Casamento : NSObject{
    
    int ID;
    NSString *noivo;
    NSString *noiva;
    NSDate *data;
    NSDecimalNumber *orcamento;
    NSMutableArray *convidados;
    NSMutableArray *tarefas;
    
}

-(void) setID:(int)novoID;
-(void) setNoivo:(NSString *)novoNoivo;
-(void) setNoiva:(NSString *)novaNoiva;
-(void) setData:(NSDate *)novaData;
-(void) setOrcamento:(NSDecimalNumber *)novoOrcamento;
-(void) setConvidados:(NSMutableArray *)novosConvidados;
-(void) setTarefas:(NSMutableArray *)novasTarefas;

-(int) getID;
-(NSString *) getNoivo;
-(NSString *) getNoiva;
-(NSDate *) getData;
-(NSDecimalNumber *) getOrcamento;
-(NSMutableArray *) getConvidados;
-(NSMutableArray *) getTarefas;

-(NSMutableDictionary *) paraNSMutableDictionary;
-(Casamento *) paraOjetoCasamentoDe:(NSDictionary *)dicionario;

@end
