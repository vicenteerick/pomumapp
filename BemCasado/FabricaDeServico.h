//
//  FabricaDeServico.h
//  BemCasado
//
//  Created by Erick Vicente on 01/05/14.
//  Copyright (c) 2014 Erick Vicente. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FabricaDeServico : NSObject

-(NSDictionary *) getPor:(NSString *)caminho;
-(NSDictionary *) postPorCaminho:(NSString *)caminho eDicionario:(NSDictionary *)objetoDicionario;
-(NSDictionary *) putPorCaminho:(NSString *)caminho eDicionario:(NSDictionary *)objetoDicionario;

@end
